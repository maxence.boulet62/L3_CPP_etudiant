//
// Created by Maxence on 23/03/2020.
//

#include "Magasin.h"
#include <iostream>

Magasin::Magasin() :
        _idCourantClient(0), _idCourantProduit(0) {}

//Clients
int Magasin::nbClients() const {
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom) {
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient++;
}

void Magasin::afficherClients() const {
    for (const auto &_client : _clients) {
        _client.afficherClient();
    }
}

void Magasin::supprimerClient(int idClient) {
    std::vector<Client> temp;
    for (const Client &_client : _clients) {
        if (_client.getId() != idClient) {
            temp.push_back(_client);
        }
    }
    if (temp.size() == _clients.size()) {
        throw std::string("ERREUR: Le client n'existe pas");
    }
    _clients.swap(temp);
}

//Produits
int Magasin::nbProduits() const {
    return _produits.size();
}

void Magasin::ajouterProduits(const std::string &nom) {
    _produits.push_back(Produit(_idCourantClient, nom));
    _idCourantProduit++;
}

void Magasin::afficherProduits() const {
    for (const auto &_produit : _produits) {
        _produit.afficherProduit();
    }
}

void Magasin::supprimerProduits(int idProduit) {
    std::vector<Produit> temp;
    for (const Produit &_produit : _produits) {
        if (_produit.getId() != idProduit) {
            temp.push_back(_produit);
        }
    }
    if (temp.size() == _produits.size()) {
        throw std::string("ERROR: Le produit n'existe pas");
    }
    _produits.swap(temp);
}

//Locations

int Magasin::nbLocations() const {
    return _locations.size();
}

void Magasin::ajouterLocations(int idClient, int idProduit) {
    Location location(idClient, idProduit);

    int i = 0;
    bool found = false;
    while (!found && i < nbLocations()) {
        if(_locations.at(i)._idClient == idClient && _locations.at(i)._idProduct == idProduit){
            found = true;
        } else {
            i++;
        }
    }
    if(found){
        throw std::string("ERREUR : ce lieu a déjà été créé.");
    } else {
        _locations.push_back(location);
    }

}

void Magasin::afficherLocations() const {
    for (const Location &_location: _locations) {
        _location.afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit) {
    std::vector<Location> temp;
    for (const Location &_location: _locations) {
        if (_location._idClient!= idClient &&
            _location._idProduct != idProduit) {
            temp.push_back(_location);
        }
    }
    if (temp.size() == _locations.size()) {
        throw std::string("ERREUR: Le produit à déjà ete crée");
    }
    _locations.swap(temp);
}

bool Magasin::trouverClientDansLocation(int idClient) {
    for (const Location &_location: _locations) {
        if (_location._idClient == idClient) {
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() {
    std::vector<int> clientLibres;
    for (const Location &_location: _locations) {
        for (const Client &_client: _clients) {
            if (trouverProduitDansLocation(_client.getId())) {
                clientLibres.push_back(_location._idClient);
            }
        }
    }
    return clientLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) {
    for (const Location &_location: _locations) {
        if (_location._idClient == idProduit) {
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() {
    std::vector<int> clientLibres;
    for (const Location &_location: _locations) {
        for (const Produit &_produit: _produits) {
            if (trouverProduitDansLocation(_produit.getId())) {
                clientLibres.push_back(_location._idProduct);
            }
        }
    }
    return clientLibres;
}



//
// Created by Maxence on 23/03/2020.
//

#ifndef TP3_MAGASIN_H
#define TP3_MAGASIN_H

#include <vector>
#include "Client.h"
#include "Produit.h"
#include "Location.h"

class Magasin {
private:
    //Vecteur
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;

    //idCourant
    int _idCourantClient;
    int _idCourantProduit;
public:
    Magasin();

    //Clients
    int nbClients() const;
    void ajouterClient(const std::string &nom);
    void afficherClients() const;
    void supprimerClient(int idClient);

    //Produits
    int nbProduits() const;
    void ajouterProduits(const std::string &nom);
    void afficherProduits() const;
    void supprimerProduits(int idProduit);

    //Locations
    int nbLocations() const;
    void ajouterLocations(int idClient, int idProduit);
    void afficherLocations() const;
    void supprimerLocation(int idClient, int idProduit);
    bool trouverClientDansLocation(int idClient);
    std::vector<int> calculerClientsLibres();
    bool trouverProduitDansLocation(int idProduit);
    std::vector<int> calculerProduitsLibres();
};


#endif //TP3_MAGASIN_H

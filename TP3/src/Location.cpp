//
// Created by Maxence on 23/03/2020.
//
#include <iostream>
#include "Location.h"

Location::Location(){
    _idClient = 0;
    _idProduct = 0;
};

Location::Location(int idClient, int idProduct) {
    _idClient = idClient;
    _idProduct = idProduct;
}

void Location::afficherLocation() const {
    std::cout << "Location (" << _idClient <<", " << _idProduct << ")" << std::endl;
}

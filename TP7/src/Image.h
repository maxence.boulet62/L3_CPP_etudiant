//
// Created by Maxence on 09/04/2020.
//

#ifndef TP_VIDE_IMAGE_H
#define TP_VIDE_IMAGE_H


#include <string>

class Image {
private :
    int _largeur;
    int _hauteur;
    int * _pixels;
public:
    Image(int largeur, int hauteur);
    Image(Image const &img);
    ~Image();
    int getLargeur() const;
    int getHauteur() const;
    void ecrirePnm(const Image & img, const std::string & nomFichier);
    void remplir(Image & img);
    Image bordure(const Image & img, int couleur, int epaisseur);

//    int& getPixel(int i, int j);
//
//     void setPixels(int i, int j, int couleur );

};


#endif //TP_VIDE_IMAGE_H

//
// Created by Maxence on 09/04/2020.
//

#include <string>
#include <fstream>
#include "Image.h"

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur) {
    _pixels=new int[largeur*hauteur];
}

Image::Image(Image const &img){
    _largeur = img.getLargeur();
    _hauteur = img.getHauteur();
    _pixels = new int [_largeur * _hauteur];
    for(int i = 0; i < _largeur*_hauteur; ++i){
            _pixels[i]=img._pixels[i];
    }
}


Image::~Image(){
    delete[](_pixels);
}

int Image::getLargeur() const {
    return _largeur;
}

int Image::getHauteur() const {
    return _hauteur;
}




void Image::ecrirePnm(const Image & img, const std::string & nomFichier){
    std::ofstream fichier(nomFichier);
    fichier <<"P2\n"<<_hauteur<<" " <<_largeur<<"\n255"<< std::endl;
    for(int i = 0; i<_largeur;i++){
        for(int j = 0; j<_hauteur;j++){
            fichier <<  _pixels[i*_largeur+j]<< " ";
        }
        fichier << std::endl;
    }
    fichier.close();
}

void Image::remplir(Image & img){
    for(int i = 0; i<_largeur;i++){
        for(int j = 0; j<_hauteur;j++){
            _pixels[j*_largeur+i]=int(((cos(i)+1)/2)*255);
        }
    }
}

//int& Image::getPixel(int i, int j){
//    return _pixels[j*_largeur+i];
//}
//
//void Image::setPixels(int i, int j, int couleur ) {
//   _pixels[j*_largeur+i]=couleur;
//}

Image Image::bordure(const Image & img, int couleur, int epaisseur){
    Image image(img);


    //border top
    for(int i = 0; i < epaisseur;i++){
        for(int j = 0; j < image.getLargeur(); j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    //border bottom
    for(int i = image.getHauteur()-epaisseur; i < image.getHauteur();i++){
        for(int j = 0; j < image.getLargeur(); j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    //border right
    for(int i = 0; i < image.getHauteur();i++){
        for(int j = image.getLargeur()-epaisseur; j < image.getLargeur(); j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    //border left
    for(int i = 0; i < image.getHauteur();i++){
        for(int j = 0; j < epaisseur; j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    return image;
}
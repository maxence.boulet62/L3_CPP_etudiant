#include "Doubler.hpp"
#include "Image.h"

#include <iostream>

int main() {
    Image im(50,50);
    std::cout << im.getHauteur() << std::endl;
    im.remplir(im);
    std::cout << im.getHauteur() << std::endl;
    im.ecrirePnm(im, "image.pnm");
    std::cout << im.getHauteur() << std::endl;
    Image im2=im.bordure(im,250,3);
    im2.ecrirePnm(im, "image2.pnm");
    im.ecrirePnm(im, "image3.pnm");
    return 0;
}


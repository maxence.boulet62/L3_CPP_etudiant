//
// Created by Maxence on 09/04/2020.
//
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "image.h"

TEST_CASE("Image", "getPixel")
{
    Image im(16,16);
    REQUIRE(im.getHauteur()==9);
    REQUIRE(im.getLargeur()==5);

}
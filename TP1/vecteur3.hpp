
#ifndef VECTEUR3_H
#define VECTEUR3_H

struct Vecteur3D {
    float x,y,z;
	 	Vecteur3D(float x, float y,float z);
	    void afficher();
	    float norme();
};

Vecteur3D init(float x, float y,float z);

void afficher(Vecteur3D v);

#endif



#include "vecteur3.hpp"

#include <iostream>
#include <cmath>


Vecteur3D init(float x, float y,float z) {
	Vecteur3D vector = {x, y, z};
	return vector;
}


void afficher(Vecteur3D v){
	std::cout<<"("<<v.x<<";"<<v.y<<";"<<v.z<<")"<<std::endl;
}


Vecteur3D::Vecteur3D(float x, float y,float z):
 	x(x), y(y), z(z)
{}

void Vecteur3D::afficher(){
     std::cout<<"("<<x<<", "<<y<<", "<<z<<")"<<std::endl;
}

float Vecteur3D::norme(){
	return std::sqrt(pow(x,2)+pow(y,2)+pow(z,2));
}
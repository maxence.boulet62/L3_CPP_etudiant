// Fibonacci_test.cpp
#include "vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Vecteur3) { };

TEST(Vecteur3, test_vecteur3) {  // premier test
    Vecteur3D vecteur3(2.0,3.0,6.0);
    CHECK_EQUAL(vecteur3.x, 2);
    CHECK_EQUAL(vecteur3.y, 3);
    CHECK_EQUAL(vecteur3.z, 6);
}

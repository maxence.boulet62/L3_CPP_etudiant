// Fibonacci_test.cpp
#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, test_fiboR) {  // premier test
    int result = fibonacciRecursif(7);
    CHECK_EQUAL(13, result);
}

TEST(GroupFibo, test_fiboI) {  // deuxième test
    int result = fibonacciIteratif(7);
    CHECK_EQUAL(13, result);
}
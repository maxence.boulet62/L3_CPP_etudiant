#include "Fibonacci.hpp"
#include <cctype>
#include <iostream>
#include "catch.hpp"

int main(){
	int nbR = fibonacciRecursif(7);
	int nbI = fibonacciIteratif(7);
	std::cout << "fibonacciIteratif " << nbI << std::endl;
	std::cout << "fibonacciRecursif " << nbR << std::endl;
	return 0;
}
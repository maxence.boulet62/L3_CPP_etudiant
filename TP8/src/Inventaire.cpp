#include "Inventaire.hpp"


std::ostream &operator <<(std::ostream &os, Inventaire &i ) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    for(const Bouteille bouteille : i._bouteilles){
        os <<bouteille<<"\n";
    }
    std::locale::global(vieuxLoc);
    return os;
}

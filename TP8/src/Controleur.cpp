#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include<string>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    //_inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    chargerInventaire("mesBouteilles.txt");
    for (auto & v : _vues)
      v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte(){
std::string bouteilles;
    for(const Bouteille bouteille : _inventaire._bouteilles){
        bouteilles= bouteilles +  bouteille._nom +";"+ bouteille._date + ";" + std::to_string(bouteille._volume) + "\n";
    }
    return bouteilles;
}

void Controleur::chargerInventaire(std::string Fichier){
    std::ifstream myfile(Fichier);

    if(myfile.is_open()){
        std::string line;
        Bouteille bouteille;

        while (getline(myfile, line)){
            myfile >> bouteille;
            _inventaire._bouteilles.push_back(bouteille);
        }
    }

    for (auto & v : _vues){
        v->actualiser();
    }
}


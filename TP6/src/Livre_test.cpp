#define CATCH_CONFIG_MAIN

#include <sstream>

#include "Livre.h"
#include "catch.hpp"



TEST_CASE("GroupLivre", "Livre_constructeur_1")
{
	Livre livre("titre1", "auteur1", 1337);
	CHECK(livre.getTitre() == std::string("titre1"));
	CHECK(livre.getAuteur() == "auteur1");
	REQUIRE(livre.getAnnee() == 1337);
}


TEST_CASE("GroupLivre1", "Livre_constructeur_2")
{
	try 
    {
        Livre livre("titre1;", "auteur1", 1337);
		FAIL( "exception non levee" );
	}
	catch (const std::string& str) 
    {
        REQUIRE(str == "erreur : titre non valide (';' non autorisé)");
	}
}

TEST_CASE("GroupLivre2", "Livre_constructeur_3")
{
	try 
    {
        Livre livre("titre1", "auteur1;", 1337);
		FAIL( "exception non levee" );
	}
	catch (const std::string& str) 
    {
        REQUIRE(str == "erreur : auteur non valide (';' non autorisé)");
	}
}

TEST_CASE("GroupLivre3", "Livre_inferieur_pp")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a0",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a0",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a0",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_pz") 
{
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a1",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a1",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a1",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_pm")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a2",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a2",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t0","a2",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_zp")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a0",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a0",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a0",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_zz")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a1",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a1",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a1",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_zm")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a2",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a2",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t1","a2",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_mp")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a0",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a0",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a0",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_mz")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a1",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a1",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a1",2));
}

TEST_CASE("GroupLivre", "Livre_inferieur_mm")
{
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a2",1));
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a2",0));
    REQUIRE(Livre("t1","a1",1)<Livre("t2","a2",2));
}

TEST_CASE("GroupLivre", "Livre_egalite_1")
{
    REQUIRE(Livre("t1","a1",1)==Livre("t1","a1",1));
}

TEST_CASE("GroupLivre", "Livre_egalite_2")
{
    REQUIRE(Livre("t1","a1",1)==Livre("t2","a1",1));
    REQUIRE(Livre("t1","a1",1)==Livre("t1","a2",1));
    REQUIRE(Livre("t1","a1",1)==Livre("t1","a1",2));
}

TEST_CASE("GroupLivre", "Livre_entree_1")
{
	Livre livre;
	std::stringstream s("titre;auteur;42");
	s >> livre;
	CHECK(livre.getTitre() == "titre");
	CHECK(livre.getAuteur() == "auteur");
	REQUIRE(livre.getAnnee() == 42);
}

TEST_CASE("GroupLivre", "Livre_sortie_1")
{
	Livre livre("titre", "auteur", 42);
	std::stringstream s;
	s << livre;
	REQUIRE(std::string("titre;auteur;42") == s.str());
}


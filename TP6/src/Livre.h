//
// Created by Maxence on 27/03/2020.
//

#ifndef TP6_LIVRE_H
#define TP6_LIVRE_H
#include <string>

class Livre {


public:
    std::string _titre;
    std::string _auteur;
    int _annee;

    Livre();

    Livre(const std::string &titre, const std::string &auteur, int annee);

    const std::string &getTitre() const;

    const std::string &getAuteur() const;

    int getAnnee() const;
    bool operator<(const Livre & l2)const;
    bool operator==(const Livre & l2)const;

};
std::ostream& operator<<(std::ostream& os, const Livre& l1);
std::istream& operator>>(std::istream& is, Livre& l1);


#endif //TP6_LIVRE_H

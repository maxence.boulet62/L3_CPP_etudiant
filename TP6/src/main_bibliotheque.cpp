
#include <iostream>
#include "Livre.h"
#include "Bibliotheque.h"

int main() {
    //livres
    Livre livre("titre1", "auteur1", 1337);
    std::cout << livre.getTitre() <<","<<livre.getAuteur() <<","<< livre.getAnnee() << std::endl;

    Livre livre2("titre2", "auteur1", 137);
    std::cout << livre2.getTitre() <<","<<livre2.getAuteur() <<","<< livre2.getAnnee() << std::endl;

    Livre test1("t1","a1",1);
    Livre test2("t2","a2",1);

    std::cout << test1.operator<(test2) ;

    //bilbio
    Bibliotheque b;
    b.push_back(Livre("t2","a2",37));
    b.push_back(livre);
    b.push_back(livre2);
    b.push_back(Livre("t1","a1",13));
    b.afficher();

    b.trierParAuteurEtTitre();
    b.afficher();

    b.trierParAnnee();
    b.afficher();

    b.ecrireFichier("bibliotheque_fichier_tmp.txt");
    Bibliotheque b2;
    b2.lireFichier("bibliotheque_fichier_tmp.txt");
    std::cout << b2[0] << " || " << b2[1];
    return 0;
}


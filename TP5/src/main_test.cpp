//
// Created by Maxence on 24/03/2020.
//
#define CATCH_CONFIG_MAIN

#include <iostream>
#include <vector>

#include "Ligne.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygonneRegulier.h"
#include "catch.hpp"


TEST_CASE("test_point", "test 1") {
    Point p1(2, 6);
    REQUIRE(p1._x== 2);
    REQUIRE(p1._y== 6);
}

TEST_CASE("test_couleur", "test 2") {
    Couleur c(150, 180, 210);
    REQUIRE(c._r== 150);
    REQUIRE(c._g== 180);
    REQUIRE(c._b== 210);
}

TEST_CASE("test_ligne", "test 3") {
    Couleur c(25, 85, 120);
    Point p1(2, 6);
    Point p2(3, 4);

    Ligne l(c, p1, p2);

    REQUIRE(l.getP0()._x == 2);
    REQUIRE(l.getP0()._y == 6);
    REQUIRE(l.getP1()._x == 3);
    REQUIRE(l.getP1()._y == 4);

    REQUIRE(l.getCouleur()._r== 25);
    REQUIRE(l.getCouleur()._g== 85);
    REQUIRE(l.getCouleur()._b== 120);

}

TEST_CASE("test_polygone", "test 4") {
    Couleur c(25, 85, 120);
    Point p1(2, 6);

    PolygonneRegulier poly(c, p1, 2, 3);

    REQUIRE(poly.getCouleur()._r== 25);
    REQUIRE(poly.getCouleur()._g== 85);
    REQUIRE(poly.getCouleur()._b== 120);

    REQUIRE(poly.getPoint(0)._x == 4);
    REQUIRE(poly.getPoint(0)._y == 6);

    REQUIRE(poly.getNbPoints() == 3);

}

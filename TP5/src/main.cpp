//
// Created by Maxence on 24/03/2020.
//

#include "Ligne.h"
#include "PolygonneRegulier.h"

#include <iostream>

//int main(){
//    Couleur c(220,25,150);
//    std::cout << " couleur b :" << c._b << std::endl ;
//
//    Point p1(1, 2);
//    Point p2(5, 5);
//
//    Ligne l(c, p1, p2);
//    l.afficher();
//
//
//    PolygonneRegulier poly(c, p1, 4, 4);
//    std::cout << "test poly point x normalement c'est 1 la rep :" << poly.getPoint(0)._x ;
//    std::cout << "test poly point y normalement c'est 2 la rep :" << poly.getPoint(0)._y ;
//    poly.afficher();
//
//return 0;
//}

#include <gtkmm.h>

class MaFenetre : public Gtk::Window {
private:
    Gtk::HPaned _paned; // un panneau d'alignement
    Gtk::Label _label; // un texte
    Gtk::Button _button; // un bouton

public:
    MaFenetre() : _paned(),
                  _label("un texte"),
                  _button("un bouton")
    {
        _paned.add1(_label); // ajoute le texte
        _paned.add2(_button); // ajoute le bouton
        add(_paned); // ajoute le panneau a la fenetre
        set_title("polygon");
        show_all(); // demande d'afficher les elements
    }
};
int main(int argc, char ** argv) {
    Gtk::Main kit(argc, argv); // application gtkmm
    MaFenetre fenetre; // fenetre principale
    Gtk::Label label(" Hello world ! "); // message
    fenetre.add(label);
    fenetre.show_all();
    kit.run(fenetre); // lance la boucle evenementielle
    return 0;
}

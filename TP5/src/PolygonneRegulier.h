//
// Created by Maxence on 24/03/2020.
//

#ifndef TP_VIDE_POLYGONNEREGULIER_H
#define TP_VIDE_POLYGONNEREGULIER_H

#include <vector>
#include "Point.h"
#include "FigureGeometrique.h"

class PolygonneRegulier: public FigureGeometrique {
private:
    int _nbPoints;
    std::vector<Point> _points;

public:
    PolygonneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes);
    virtual void afficher() const;
    int getNbPoints() const;
    const Point &getPoint(int indice) const;
};


#endif //TP_VIDE_POLYGONNEREGULIER_H

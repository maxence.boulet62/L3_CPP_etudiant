//
// Created by Maxence on 23/03/2020.
//

#ifndef TP4_FIGUREGEOMETRIQUE_H
#define TP4_FIGUREGEOMETRIQUE_H

#include "Point.h"
#include "Couleur.h"

class FigureGeometrique {
protected :
    Couleur _couleur;
public :
    FigureGeometrique(const Couleur couleur);

    const Couleur &getCouleur() const;

};


#endif //TP4_FIGUREGEOMETRIQUE_H

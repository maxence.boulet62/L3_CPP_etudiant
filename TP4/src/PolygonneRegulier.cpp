//
// Created by Maxence on 24/03/2020.
//

#include <cmath>
#include <iostream>
#include <string>
#include "PolygonneRegulier.h"
#include "Point.h"


PolygonneRegulier::PolygonneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes) :
        FigureGeometrique(couleur), _nbPoints(nbCotes) {

    for (int i = 0; i < _nbPoints; ++i) {
        float theta = i * 2 * 3.14 / (float) _nbPoints;
        int x = rayon * cos(theta) + centre._x;
        int y = rayon * sin(theta) + centre._y;
        _points.push_back(Point(x, y));
    }
}

void PolygonneRegulier::afficher() const {
    std::string message = "";

    for (Point _point : _points) {
        message = message + " " + std::to_string(_point._x) + "_" + std::to_string(_point._y);
    }

    std::cout << "PolygoneRegulier: " << getCouleur()._r << "_" << getCouleur()._g << "_" << getCouleur()._b << message << std::endl;
}

int PolygonneRegulier::getNbPoints() const {
    return _nbPoints;
}

const Point &PolygonneRegulier::getPoint(int indice) const{
    return _points[indice];
}

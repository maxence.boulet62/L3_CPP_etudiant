//
// Created by Maxence on 24/03/2020.
//

#ifndef TP_VIDE_LIGNE_H
#define TP_VIDE_LIGNE_H

#include "FigureGeometrique.h"
#include "Point.h"

class Ligne : public FigureGeometrique{
protected:
    Point _p0;
    Point _p1;
public:
    Ligne(const Couleur &couleur, const Point &p0, const Point &p1);
    virtual void afficher() const;

    const Point &getP0() const;
    const Point &getP1() const;
};


#endif //TP_VIDE_LIGNE_H

//
// Created by Maxence on 23/03/2020.
//

#ifndef TP4_COULEUR_H
#define TP4_COULEUR_H
#include <iostream>


struct Couleur {
    double _r;
    double _g;
    double _b;

    Couleur(double r,double g,double b);


};


#endif //TP4_COULEUR_H

//
// Created by Maxence on 23/03/2020.
//

#include "FigureGeometrique.h"

FigureGeometrique::FigureGeometrique(const Couleur couleur): _couleur(couleur){}

const Couleur &FigureGeometrique::getCouleur() const {
    return _couleur;
}

//
// Created by Maxence on 24/03/2020.
//

#include "Ligne.h"
#include "PolygonneRegulier.h"

#include <iostream>



int main(){
    Couleur c(220,25,150);
    std::cout << " couleur b :" << c._b << std::endl ;

    Point p1(1, 2);
    Point p2(5, 5);

    Ligne l(c, p1, p2);
    l.afficher();


    PolygonneRegulier poly(c, p1, 4, 4);
    std::cout << "test poly point x normalement c'est 1 la rep :" << poly.getPoint(0)._x ;
    std::cout << "test poly point y normalement c'est 2 la rep :" << poly.getPoint(0)._y ;
    poly.afficher();

    return 0;
}